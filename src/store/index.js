import { createStore, applyMiddleware, compose } from "redux"
import loggerMiddleware from "redux-logger"
import rootReducer from "../reducers"

export default function cofigureStore() {
  if (process.env.NODE_ENV === "production") {
    return createStore(rootReducer)
  }

  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(loggerMiddleware))
  )

  if (module.hot) {
    module.hot.accept("../reducers", () => {
      store.replaceReducer(rootReducer)
    })
  }

  return store
}
