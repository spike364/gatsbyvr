import { combineReducers } from "redux"

const initialState = {
  excludedTimes: [],
  excludedDates: [],
  selectedDate: new Date(),
  selectedTime: null,
  selectedSpot: null,
}

const excludedTimes = (state = initialState.excludedTimes, action) => {
  switch (action.type) {
    case types.GET_EXCLUDED_TIMES:
      return action.payload.excludedTimes
    default:
      return state
  }
}

const excludedDates = (state = initialState.excludedDates, action) => {
  switch (action.type) {
    case types.GET_EXCLUDED_DATES:
      return action.payload.excludedDates
    default:
      return state
  }
}

const selectedSpot = (state = initialState.selectedSpot, action) => {
  switch (action.type) {
    case types.SET_SELECTED_SPOT:
      return action.payload.selectedSpot
    default:
      return state
  }
}

export default combineReducers({
  selectedSpot,
  excludedDates,
  excludedTimes,
})
