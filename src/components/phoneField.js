import React from "react"
// import InputMask from "react-input-mask"
import calcStyles from "./calc.module.css"

const PhoneField = props => {
  const { setPhone, register, phoneError } = props

  return (
    <div className={calcStyles.fieldGroup}>
      <label className={calcStyles.fieldLabel} htmlFor="phone">
        Телефон
      </label>
      <input
        // mask="+7 999 999 99 99"
        maskchar=" "
        name="phone"
        type="tel"
        ref={register({
          required: true,
          minLength: 6,
          maxLength: 20,
          pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/,
        })}
        className={calcStyles.fieldInput}
        onBlur={e => setPhone(e.target.value)}
      />
      {phoneError && (
        <div className={calcStyles.fieldError}>
          Введите правильный номер телефона
        </div>
      )}
    </div>
  )
}
export default PhoneField
