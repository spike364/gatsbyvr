import React from "react"
import calcStyles from "./calc.module.css"

const BonusTime = props => {
  const { bonusTime } = props

  return (
    <div className={calcStyles.fieldGroup}>
      <div className={calcStyles.fieldLabel}>Бонусное время</div>
      <div className={calcStyles.bonusTime}>
        {bonusTime && bonusTime + " минут"}
      </div>
    </div>
  )
}

export default BonusTime
