import React, { useEffect, useState, useContext } from "react"
import DatePicker, { registerLocale } from "react-datepicker"
import ru from "date-fns/locale/ru"
import calcStyles from "./calc.module.css"
import DateContext from "./DateContext"
import { set, parseISO } from "date-fns"

const DateSelector = props => {
  const {
    spot,
    date,
    dateError,
    register,
    minTime,
    maxTime,
    currentDate,
  } = props

  const [excludedTimes, setExcludedTimes] = useState()

  const fetchReservedTime = async (spot, date) => {
    try {
      const response = await fetch(
        `http://localhost:3000/reservations/dates?spot=${spot.name}&date=${date}`
      ).then(response => response.json())
      setExcludedTimes([
        ...new Set(response.flat().map(date => parseISO(date))),
      ])
    } catch (e) {
      console.log(e)
    }
  }

  const handleDateChange = (date = new Date()) => {
    fetch(
      "https://isdayoff.ru/" +
        date
          .toISOString()
          .slice(0, 10)
          .replace(/-/g, "")
    )
      .then(res => res.json())
      .then(data => {
        data === 1
          ? dateApi.setDayType("WEEKEND")
          : dateApi.setDayType("WEEKDAY")
      })

    dateApi.setDate(date)
    fetchReservedTime(spot, date)
  }

  registerLocale("ru", ru)

  const dateApi = useContext(DateContext)

  return (
    <div className={calcStyles.fieldGroup + " " + calcStyles.calendar}>
      <label className={calcStyles.fieldLabel} htmlFor="date">
        Дата и время
      </label>
      <DatePicker
        selected={date}
        onChange={date => {
          handleDateChange(date)
        }}
        showTimeSelect
        timeFormat="HH:mm"
        timeIntervals={5}
        timeCaption="Время"
        locale={ru}
        // excludeDates={dates.dates}
        excludeTimes={excludedTimes}
        minDate={currentDate}
        minTime={minTime}
        maxTime={maxTime}
        dateFormat="dd.MM.yyyy, hh:mm"
        className={calcStyles.fieldInput}
        name="date"
        ref={register({ required: true })}
      />
      {dateError && (
        <div className={calcStyles.fieldError}>
          Необходимо выбрать дату и время игры
        </div>
      )}
    </div>
  )
}
export default DateSelector
