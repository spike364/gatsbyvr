import React from "react"
import ButtonStyles from "./Button.module.css"

const Button = React.memo(props => {
  const { size, color, type, action, text } = props
  return (
    <button className={ButtonStyles.Button} action={action}>
      {text}
    </button>
  )
})

export default Button
