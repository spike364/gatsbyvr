import React, { useContext } from "react"
import calcStyles from "./calc.module.css"
import SpotContext from "./SpotContext"

const SpotSelector = props => {
  const { spots, trcError, register } = props
  const spotApi = useContext(SpotContext)

  return (
    <div className={calcStyles.fieldGroup}>
      <label className={calcStyles.fieldLabel} htmlFor="trc">
        Расположение клуба
      </label>
      <select
        id="trc"
        className={calcStyles.fieldInput}
        onChange={e => {
          spotApi.setSpot(spots[e.target.value])
        }}
        name="trc"
        ref={register({
          required: true,
        })}
      >
        {spots.map((spot, i) => (
          <option key={i} value={i}>
            {spot.name}
          </option>
        ))}
      </select>
      {trcError && "Необходимо выбрать локацию"}
    </div>
  )
}

export default SpotSelector
