import React from "react"
import calcStyles from "./calc.module.css"

const PhoneField = props => {
  const { setName, register, nameError } = props

  return (
    <div className={calcStyles.fieldGroup}>
      <label className={calcStyles.fieldLabel} htmlFor="name">
        Имя
      </label>
      <input
        type="text"
        id="name"
        name="name"
        ref={register({ required: true })}
        className={calcStyles.fieldInput}
        onChange={e => setName(e.target.value)}
        autoFocus
      />
      {nameError && (
        <div className={calcStyles.fieldError}>Необходимо ввести имя</div>
      )}
    </div>
  )
}
export default PhoneField
