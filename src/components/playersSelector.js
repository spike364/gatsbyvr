import React from "react"
import calcStyles from "./calc.module.css"

const PlayerSelector = props => {
  const { spot, setPlayers, players, register, playersError } = props
  const playersCollection = [
    ...new Set(spot.tariffs.map(tariff => tariff.players)),
  ]

  const adjustedPlayers =
    playersCollection.indexOf(Number(players)) < 0
      ? playersCollection[0]
      : players

  return (
    <div className={calcStyles.fieldGroup}>
      <label className={calcStyles.fieldLabel}>Количество игроков</label>
      <div className={calcStyles.radioFieldGroupContainer}>
        {playersCollection.map((tariffPlayers, key) => (
          <div className={calcStyles.radioFieldGroup} key={key}>
            <input
              type="radio"
              value={tariffPlayers}
              className={calcStyles.fieldInput}
              checked={tariffPlayers == adjustedPlayers}
              id={`playersRadio-${key}`}
              onChange={e => setPlayers(e.target.value)}
              name="players"
              ref={register({
                required: true,
              })}
            />
            <label
              className={calcStyles.radioFieldLabel}
              htmlFor={`playersRadio-${key}`}
            >
              {Number(tariffPlayers) === 1
                ? tariffPlayers + " игрок"
                : tariffPlayers + " игрока"}{" "}
            </label>
          </div>
        ))}
      </div>
      {playersError && (
        <div className={calcStyles.fieldError}>
          Необходимо выбрать количество игроков
        </div>
      )}
    </div>
  )
}

export default PlayerSelector
