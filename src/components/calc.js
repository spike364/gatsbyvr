import React, { useState, useEffect } from "react"

import Firebase from "firebase"
import config from "./config"
import { useForm } from "react-hook-form"
import format from "date-fns/format"
import spotsLocal from "../newspots.json"
import "react-datepicker/dist/react-datepicker.css"
import calcStyles from "./calc.module.css"
import PlaytimeSelection from "./PlaytimeSelection"
import SpotSelector from "./SpotSelector"
import DateSelector from "./DateSelector"
import PlayersSelector from "./PlayersSelector"
import PhoneField from "./PhoneField"
import NameField from "./NameField"
import TotalPrice from "./TotalPrice"
import BonusTime from "./BonusTime"
import Button from "./atoms/Button"
import SpotContext from "./SpotContext"
import DateContext from "./DateContext"

Firebase.initializeApp(config)

const spotsRef = Firebase.database().ref("/")

const Calc = ({ initialSpotNumber = 0 }) => {
  const { register, handleSubmit, errors } = useForm()
  const onSubmit = data => {
    data.date = format(date, "dd.MM.yyyy hh:mm")
    data.spot = "ТРК " + spot.name
    data.players = players
    data.trc = spot.name
    data["action-type"] = "calc"
    data.phone = phone
    data.name = name
    data.product = "game"
    data.price = spot.tariffs.filter(
      tariff => tariff.playtime == playtime && tariff.players == players
    )[0].price
    data.bonusTime = spot.tariffs.filter(
      tariff => tariff.playtime == playtime && tariff.players == players
    )[0].bonusTime
    let queryData = Object.keys(data)
      .map(key => key + "=" + data[key])
      .join("&")
    window.location.href = `https://mir-vr.com/payment.php?${btoa(
      unescape(encodeURIComponent(queryData))
    )}`
  }
  const currentDate = new Date()
  const currentDayType =
    currentDate.getDay() === 6 || currentDate.getDay() === 0
      ? "WEEKEND"
      : "WEEKDAY"

  let minTime = currentDate.setHours(currentDate.getHours() + 1)
  let maxTime = currentDate.setHours(22)

  const isToday = (date = new Date()) => {
    const today = new Date()
    return (
      date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear()
    )
  }

  if (!isToday(date)) {
    minTime = currentDate.setHours(10)
  }

  const [spots, setSpots] = useState(spotsLocal)
  const [spot, setSpot] = useState(spots[0])
  const [playtime, setPlaytime] = useState(spots[0].tariffs[0].playtime)
  const [players, setPlayers] = useState(spots[0].tariffs[0].players)
  const [name, setName] = useState("")
  const [phone, setPhone] = useState("")
  const [date, setDate] = useState(currentDate)
  const [dayType, setDayType] = useState(currentDayType)
  const [theme, setTheme] = useState("light")

  const spotApi = { setSpot }
  const dateApi = { setDate, setDayType }

  useEffect(() => {
    fetchReservedTime(spot.slug)
  }, [spot])

  useEffect(() => {
    spotsRef.on(
      "value",
      snapshot => {
        setSpots(snapshot.val())
      },
      error => {
        console.error(error)
      }
    )
  }, [])

  const fetchReservedTime = async spot => {
    const reservationData = { name: spot.name, date: date }
    try {
      const response = await fetch("http://localhost:3000/reservations/dates", {
        reservationData,
      }).then(response => response.json())
      console.log(response)
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <React.Fragment>
      <div className={calcStyles.calcWrapper + " " + calcStyles[theme]}>
        <div className={calcStyles.formHeader}>Рассчитайте стоимость игры</div>
        <form
          className={calcStyles.fieldsWrapper}
          onSubmit={handleSubmit(onSubmit)}
        >
          <NameField
            setName={setName}
            register={register}
            nameError={errors.name}
          />

          <PhoneField
            setPhone={setPhone}
            register={register}
            phoneError={errors.phone}
          />

          <SpotContext.Provider value={spotApi}>
            <SpotSelector
              spots={spots}
              trcError={errors.trc}
              setSpot={setSpot}
              register={register}
            />
          </SpotContext.Provider>

          <DateContext.Provider value={dateApi}>
            <DateSelector
              date={date}
              spot={spot}
              setDate={setDate}
              register={register}
              minTime={minTime}
              maxTime={maxTime}
              currentDate={currentDate}
              dateError={errors.date}
            />
          </DateContext.Provider>

          <PlaytimeSelection
            setPlaytime={setPlaytime}
            playtime={playtime}
            spot={spot}
            playtimeError={errors.playtime}
            dayType={dayType}
            register={register}
          />

          <PlayersSelector
            setPlayers={setPlayers}
            players={players}
            spot={spot}
            register={register}
            dayType={dayType}
            playersError={errors.players}
          />

          <TotalPrice
            totalPrice={
              spot.tariffs.filter(
                tariff =>
                  tariff.playtime == playtime && tariff.players == players
              )[0]?.price
            }
          />
          {/* {spot.tariffs.map(tariff => {
              console.log(tariff.playtime + " ::: " + playtime)
              console.log(tariff.players + " ::: " + players)
            })} */}

          <BonusTime
            bonusTime={
              spot.tariffs.filter(
                tariff =>
                  tariff.playtime == playtime && tariff.players == players
              )[0].bonusTime
            }
          />

          <div className={calcStyles.fieldGroupButton}>
            <Button
              size="xl"
              color="purple"
              type="empty"
              text="Записаться на игру"
            />
          </div>
        </form>
      </div>
    </React.Fragment>
  )
}

export default Calc
