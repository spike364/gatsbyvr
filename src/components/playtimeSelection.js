import React from "react"
import calcStyles from "./calc.module.css"

const PlaytimeSelection = props => {
  const {
    spot,
    playtimeError,
    setPlaytime,
    playtime,
    dayType,
    register,
  } = props

  const playtimeCollection = [
    ...new Set(
      spot.tariffs
        .filter(tariff => tariff.dayType === dayType)
        .map(tariff => tariff.playtime)
    ),
  ]

  const adjustedPlaytime =
    playtimeCollection.indexOf(Number(playtime)) < 0
      ? playtimeCollection[0]
      : playtime

  return (
    <div className={calcStyles.fieldGroup}>
      <label className={calcStyles.fieldLabel}>Продолжительность</label>
      <div className={calcStyles.radioFieldGroupContainer}>
        {playtimeCollection.map((tariffPlaytime, key) => (
          <div className={calcStyles.radioFieldGroup} key={key}>
            <input
              name="playtime"
              ref={register({
                required: true,
              })}
              type="radio"
              className={calcStyles.fieldInput}
              value={tariffPlaytime}
              id={`playtimeRadio-${key}`}
              checked={tariffPlaytime == adjustedPlaytime}
              onChange={e => setPlaytime(e.target.value)}
            />
            <label
              className={calcStyles.radioFieldLabel}
              htmlFor={`playtimeRadio-${key}`}
            >
              {tariffPlaytime} мин.
            </label>
          </div>
        ))}
      </div>
      {playtimeError && (
        <div className={calcStyles.fieldError}>
          Необходимо выбрать время игры
        </div>
      )}
    </div>
  )
}
export default PlaytimeSelection
